
package br.com.senac;


public class Triangulo extends FiguraGeometrica{
    private double base;
    private double altura;

    public Triangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    @Override
    public double getArea() {
       double resultado;
       resultado = (this.base * altura) /2 ;
       
       return resultado;
    }
    
    
   
}
