/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

public class Quadrado extends FiguraGeometrica{
    
    private double lado;

    public Quadrado(double lado) {
        this.lado = lado;
    }
    
    
    
    @Override
    public double getArea() {
        double result;
        result = this.lado * lado;
        
        return result;
    }
    
    
    
    
    
}
