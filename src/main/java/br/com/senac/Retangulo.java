package br.com.senac;


public class Retangulo extends FiguraGeometrica{
    private double base;
    private double altura;

    public Retangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }
    
    
    
    @Override
    public double getArea() {
        double resultado;
        resultado = this.altura * base;
        
        return resultado;
    }
    
   
}
