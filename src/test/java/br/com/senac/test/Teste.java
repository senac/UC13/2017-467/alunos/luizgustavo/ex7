/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.Circulo;
import br.com.senac.Quadrado;
import br.com.senac.Retangulo;
import br.com.senac.Triangulo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author sala304b
 */
public class Teste {
    
    public Teste() {
    }
    
    @Test
    public void calcularAreaTrianguloTest(){
        Triangulo triangulo = new Triangulo(10, 20);
        
        assertEquals(100, triangulo.getArea(), 0.01);
      
        
    }
    @Test
    public void calcularAreaQuadradoTest(){
        Quadrado quadrado = new Quadrado(20);
        
        assertEquals(400, quadrado.getArea(), 0.01);
    }
    @Test
    public void calcularAreaRetanguloTest(){
        Retangulo retangulo = new Retangulo(5, 5);
        
        assertEquals(25, retangulo.getArea(),0.01);
        
    }
    @Test
    public void calcularAreaCirculoTest(){
        Circulo circulo = new Circulo(10);
        
        assertEquals(31.4, circulo.getArea(), 0.01);
    }
}
